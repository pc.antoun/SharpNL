#SharpNL

## What is this?

> An awesome and independent reimplementation of the [Apache OpenNLP] software library in C#

> [![NuGet](https://img.shields.io/nuget/v/Knuppe.MA.SharpNL.svg)](https://www.nuget.org/packages/Knuppe.MA.SharpNL/)

## Main features/characteristics

> - Fully C# managed [.net standard](https://github.com/dotnet/standard) library!
> - Fully compatible with the OpenNLP models (1.5, 1.5.3 and 1.6).
> - Was built from scratch by hand, without any assist tool in order to maximize the synergy with .net technology.
> - There are [analyzers](https://github.com/knuppe/SharpNL/wiki/Analyzers) that help a lot the implementation and abstraction of this library.  
> - The heavy operations (like training) can be monitored and cancelled.
> - Some file formats were revamped/improved (Ad and Penn Treebank).
> - The deprecated methods from OpenNLP were not ported!
> - English inflection tools.
> - The library targets .Net Standard 2.1, .Net 6, and .Net Framework 4.6.2

## TODO

> :heart: [How to contribute](contributing.md)

## License

> - The library's license is [Apache-2.0](https://licenses.nuget.org/Apache-2.0) and no additional license. All thanks to Knuppe!

## Support

[![donate](resources/donate.gif)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=7SWNPAPJNSARC)

[Original NuGet Package]: https://www.nuget.org/packages/Knuppe.SharpNL/
[New NuGet Package]: https://www.nuget.org/packages/Knuppe.MA.SharpNL/
[Apache OpenNLP]: http://opennlp.apache.org
